package model

import java.time.LocalDateTime
import org.scalatest.funsuite.AnyFunSuite

import scala.util.{Failure, Success}

class VivaScheduleTest extends AnyFunSuite{

  val viva = new Viva("Title 1", "Student 001", List(VivaResource("T001",President),
                          VivaResource("T002",Adviser),VivaResource("E002", Supervisor), VivaResource("E003", Supervisor)))
  val availabilityInterval = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T15:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),20)
  val vivaScheduleList = List(Success(VivaSchedule(viva,availabilityInterval)))

  val vivaScheduleListFailure = List(Failure(new IllegalArgumentException("Viva "+viva.title+" cannot be scheduled")))
  val listVivaResources= List(Teacher("T001","Teacher 001", null),Teacher("T002","Teacher 002", null),External("E002","External 002", null),External("E003","External 003", null))

  test("VivaSchedule.toXml") {
    val expected = true
    val expected2 =
      <schedule xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../schedule.xsd"
                totalPreference="20">
        <viva student="Student 001" title="Title 1" start="2020-05-30T15:30:00" end="2020-05-30T17:30:00" preference="20">
          <president name="Teacher 001"/>
          <adviser name="Teacher 002"/>
          <supervisor name="External 002"/>
          <supervisor name="External 003"/>
        </viva>
      </schedule>

    val result = VivaSchedule.toXml(vivaScheduleList,listVivaResources).isSuccess
    assert(expected === result)
  }

  test("Negative VivaSchedule.toXml") {
    val expected = false
    val result = VivaSchedule.toXml(vivaScheduleListFailure,listVivaResources)
    assert(expected == result.isSuccess)
  }
}
