package model

sealed trait Role {
  def desc : String
}

case object President extends Role {
  val desc = "president"
}
case object Adviser extends Role {
  val desc = "adviser"
}

case object Coadviser extends Role {
  val desc = "coadviser"
}

case object Supervisor extends Role {
  val desc = "supervisor"
}

