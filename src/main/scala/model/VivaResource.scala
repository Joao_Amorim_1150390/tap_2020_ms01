package model

import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, Node}

case class VivaResource( resource: String, role: Role){

}
object VivaResource{

  def createVivaResource(resource: String, role: Role): Try[Option[VivaResource]] = {
      Success(Some(VivaResource(resource, role)))
  }
  private def nodeToVivaResource(role: String): Option[Role] ={
    role match {
      case "president" => Some(President)
      case "adviser" => Some(Adviser)
      case "coadviser" => Some(Coadviser)
      case "supervisor" => Some(Supervisor)
      case _ => None
    }
  }
  /**
    * With validations
    * @param elem
    * @return
    */
  def fromXml(elem: Elem): Try[Option[VivaResource]] = {
    val role = this.nodeToVivaResource(elem.label)
    val resource = (elem \@ "id").toString()
    if (!role.isDefined){
      Failure(new IllegalArgumentException("Role undefined"))
    }else{
      this.createVivaResource(resource,role.get)
    }
  }
}
