package assessment

import java.time.{Duration, LocalTime}

import model._
import domain.schedule._

import scala.util.{Failure, Try}
import scala.xml.Elem

object AssessmentMS01 extends Schedule {

  /**
    * This method will validate if are there the necessary resources to accomplish the Viva in the first step.
    * Starting with obtaining the first resource booked availabilities, and from there on we check the not booked availabilites for that resource.
    * The final step is finding the possible combination of availability intervals between the needed resources for the Viva in the loop.
    * If a combination is found, the interval is successfully created in the caller method scheduleVivas
    * @param scheduledVivas
    * @param resources
    * @param duration
    * @return AvailabilityInterval
    */
  def schedulerFCFS(scheduledVivas: List[VivaSchedule], resources: List[Resource], duration: Duration) : Try[Option[AvailabilityInterval]] = {
    if(resources.isEmpty){
      Failure(new IllegalArgumentException("Resources are empty, cannot proceed with distribution"))
    }else{
      val firstResourceBookedAvailabilities = scheduledVivas.filter(_.viva.jury.exists(_.resource.equals(resources(0).id))).map(_.availabilityInterval)
      val firstResourceAvailabilities = resources(0).getNotBookedAvailabilities(firstResourceBookedAvailabilities)
      val availability = resources.drop(1).foldLeft(firstResourceAvailabilities.map(Option(_))){
        case (firstListAvailabilities, resource) =>
          val resourceBookedAvailabilities = scheduledVivas.filter(_.viva.jury.exists(_.resource.equals(resource.id))).map(_.availabilityInterval)
          resource.cross(resourceBookedAvailabilities,firstListAvailabilities,duration)
      }.find(_.isDefined).getOrElse(None)
      if(availability.isDefined){
        AvailabilityInterval.createInterval(availability.get.start,availability.get.start.plus(duration),availability.get.preference)
      }else{
        Failure(new IllegalArgumentException("Availability Interval cannot be created"))
      }
    }
  }
  /**
    * For each Viva we will apply the first come first serve methodology
    * sv -- Empty List  --------- v --individual Viva
    * Starting with an empty list of scheduled vivas that is going to be filled while going through the read Viva List
    * @param vivas
    * @param resources
    * @param duration
    * @return
    */
  def scheduleVivas(vivas: List[Viva], resources: List[Resource], duration: Duration): List[Try[VivaSchedule]] = {
    vivas.foldLeft(List[Try[VivaSchedule]]()){
      case (scheduledVivas, viva) =>
        val availability = this.schedulerFCFS(
          scheduledVivas.filter(_.isSuccess).map(_.get),
          resources.filter(r => viva.jury.exists(_.resource.equals(r.id))),
          duration)
        if(!availability.isSuccess) {
          scheduledVivas :+ Failure(new IllegalArgumentException("Viva "+viva.title+" cannot be scheduled"))
        } else{
          scheduledVivas :+ VivaSchedule.create(viva,availability.get.get)
        }
    }
  }
  // TODO: Use the functions in your own code to implement the assessment of ms01
  def create(xml: Elem): Try[Elem] = {
    val duration = Duration.between(LocalTime.MIN,LocalTime.parse((xml \@ "duration").toString()))
    //Read vivas from input xml
    val vivas = (xml \\ "vivas" \ "viva").map(Viva.fromXml(_)).toList
    val failure = vivas.find(_.isFailure)
    if(failure.isDefined) {
      failure.get match {
        case Failure(exception) => Failure(exception)
      }
    } else {
      // Read all resources from input xml
      val resources = (xml \"resources" \"teachers" \ "teacher").map(Teacher.fromXml(_)).toList.appendedAll((xml \"resources" \"externals" \"external").map(External.fromXml(_)).toList)
      VivaSchedule.toXml(scheduleVivas(vivas.map(_.get), resources.filter(_.isDefined).map(_.get), duration),resources.filter(_.isDefined).map(_.get))
    }
  }
}

