package model

import java.time.{Duration, LocalDateTime, LocalTime}

import model.{AvailabilityInterval, Teacher}
import org.scalatest.funsuite.AnyFunSuite

class ResourceTest extends AnyFunSuite {

  val duration = Duration.between(LocalTime.MIN,LocalTime.parse("01:00:00"))

  val interval1 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T13:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),1)
  val interval2 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T09:00:00"),LocalDateTime.parse("2020-05-30T12:30:00"),1)
  val freeAvailabilities = List(Some(interval1),Some(interval2))

  val interval3 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T13:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),1)
  val interval4 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T09:00:00"),LocalDateTime.parse("2020-05-30T12:30:00"),1)
  val bookedIntervals = List(interval1)


  val r2BookedPartialInterval1 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T13:30:00"),LocalDateTime.parse("2020-05-30T14:30:00"),1)
  val r2BookedPartialInterval2 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T09:00:00"),LocalDateTime.parse("2020-05-30T10:00:00"),1)
  val bookedIntervalsCross = List(r2BookedPartialInterval1,r2BookedPartialInterval2)

  val r2BookedInterval1 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T13:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),1)
  val r2BookedInterval2 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T09:00:00"),LocalDateTime.parse("2020-05-30T12:30:00"),1)
  val bookedEntireIntervals = List(r2BookedInterval1,r2BookedInterval2)


  val crossedInterval1 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T14:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),2)
  val crossedInterval2 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T10:00:00"),LocalDateTime.parse("2020-05-30T12:30:00"),2)


  val resource = Teacher("T001","Teacher 001",List(interval1,interval2))
  val resource2 = Teacher("T002","Teacher 002",List(interval3,interval4))
  val node = <teacher id="T001" name="Teacher 001">
    <availability start="2020-05-30T13:30:00" end="2020-05-30T17:30:00" preference="1"/>
    <availability start="2020-05-30T09:00:00" end="2020-05-30T12:30:00" preference="1"/>
  </teacher>


  test("Resource.cross") {
    val expected = List(Some(crossedInterval1), None, None, Some(crossedInterval2))
    val result = resource2.cross(bookedIntervalsCross,freeAvailabilities,duration)
    assert(expected === result)
  }

  test("Negative Resource.cross") {
    val expected = true
    val result = resource2.cross(bookedEntireIntervals,freeAvailabilities,duration).isEmpty
    assert(expected === result)
  }

  test("Resource.getNotBookedAvailabilities") {
    val expected = List(interval2)
    val result = resource.getNotBookedAvailabilities(bookedIntervals)
    assert(expected === result)
  }

  test("Resource.fromXml") {
    val expected = Some(Teacher("T001", "Teacher 001", List(interval1, interval2)))
    val result = Teacher.fromXml(node)
    assert(expected === result)
  }
}