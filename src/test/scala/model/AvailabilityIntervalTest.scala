package model

import java.time.{Duration, LocalDateTime, LocalTime}

import model.AvailabilityInterval
import org.scalatest.funsuite.AnyFunSuite

import scala.util.{Failure, Success}

class AvailabilityIntervalTest extends AnyFunSuite {

  val duration = Duration.between(LocalTime.MIN,LocalTime.parse("01:00:00"))
  val secondDuration = Duration.between(LocalTime.MIN,LocalTime.parse("01:30:00"))
  val thirdDuration = Duration.between(LocalTime.MIN,LocalTime.parse("03:00:00"))
  val baseInterval = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T15:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),5)
  val intervalToCompareOverlaps = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T16:30:00"),LocalDateTime.parse("2020-05-30T18:00:00"),1)
  val intervalToCompareFinished = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T16:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),1)
  val intervalToCompareContains = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T16:00:00"),LocalDateTime.parse("2020-05-30T17:00:00"),2)
  val intervalToCompareStarts = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T15:30:00"),LocalDateTime.parse("2020-05-30T18:00:00"),2)
  val crossValidationsInterval = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T13:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),3)


  val intervalToCompareError1 = new AvailabilityInterval(LocalDateTime.parse("2020-05-30T15:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),5)
  val validInterval = <availability start="2020-05-30T13:30:00" end="2020-05-30T17:30:00" preference="3"/>
  val invalidInterval = <availability start="2020-05-30T08:00:00" end="2020-05-30T12:30:00" preference="-1"/>
  val invalidInterval2 = <availability start="2020-05-30T09:00:00" end="2020-05-30T08:30:00" preference="5"/>


  test("AvailabilityInterval.overlaps") {
    val expected = true
    val result = baseInterval.overlaps(intervalToCompareOverlaps)
    assert(expected === result)
  }

  test("AvailabilityInterval.overlaps 2") {
    val expected = Some(AvailabilityInterval(LocalDateTime.parse("2020-05-30T16:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),6))
    val result = baseInterval.overlaps(intervalToCompareOverlaps,duration)
    assert(expected === result)
  }
  test("Negative AvailabilityInterval.overlaps") {
    val expected = None
    val result = baseInterval.overlaps(intervalToCompareOverlaps, secondDuration)
    assert(expected === result)
  }


  test("AvailabilityInterval.finishedBy") {
    val expected = true
    val result = baseInterval.finishedBy(intervalToCompareFinished)
    assert(expected === result)
  }

  test("AvailabilityInterval.finishedBy 2") {
    val expected = Some(AvailabilityInterval(LocalDateTime.parse("2020-05-30T16:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),6))
    val result = baseInterval.overlaps(intervalToCompareOverlaps,duration)
    assert(expected === result)
  }
  test("Negative AvailabilityInterval.finishedBy 2") {
    val expected = None
    val result = baseInterval.overlaps(intervalToCompareOverlaps,secondDuration)
    assert(expected === result)
  }

  test("AvailabilityInterval.contains 2") {
    val expected = true
    val result = baseInterval.contains(intervalToCompareContains)
    assert(expected === result)
  }
  test("AvailabilityInterval.contains ") {
    val expected = Some(AvailabilityInterval(LocalDateTime.parse("2020-05-30T16:00:00"),LocalDateTime.parse("2020-05-30T17:00:00"),7))
    val result = baseInterval.contains(intervalToCompareContains,duration)
    assert(expected === result)
  }

  test("Negative AvailabilityInterval.contains 2") {
    val expected = None
    val result = baseInterval.contains(intervalToCompareContains,secondDuration)
    assert(expected === result)
  }

  test("AvailabilityInterval.starts") {
    val expected = true
    val result = baseInterval.starts(intervalToCompareStarts)
    assert(expected === result)
  }
  test("AvailabilityInterval.starts 2") {
    val expected = Some(AvailabilityInterval(LocalDateTime.parse("2020-05-30T15:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),7))
    val result = baseInterval.starts(intervalToCompareStarts, duration)
    assert(expected === result)
  }

  test("AvailabilityInterval.starts Negative") {
    val expected = None
    val result = baseInterval.starts(intervalToCompareStarts, thirdDuration)
    assert(expected === result)
  }

  test("AvailabilityInterval.equals") {
    val expected = true
    val result = baseInterval.equals(baseInterval)
    assert(expected === result)
  }
  test("AvailabilityInterval.equals 2") {
    val expected = Some(AvailabilityInterval(LocalDateTime.parse("2020-05-30T15:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),10))
    val result = baseInterval.equals(baseInterval,duration)
    assert(expected === result)
  }
  test("Negative AvailabilityInterval.equals") {
    val expected = None
    val result = baseInterval.equals(baseInterval,thirdDuration)
    assert(expected === result)
  }

  test("AvailabilityInterval.crossValidations") {
    val expected = Some(AvailabilityInterval(LocalDateTime.parse("2020-05-30T15:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),8))
    val result = baseInterval.crossValidations(crossValidationsInterval,duration)
    assert(expected === result)
  }

  test("Negative AvailabilityInterval.crossValidations") {
    val expected = None
    val result = baseInterval.crossValidations(crossValidationsInterval,thirdDuration)
    assert(expected === result)
  }

  test("AvailabilityInterval.fromXml") {
    val expected = Some(AvailabilityInterval(LocalDateTime.parse("2020-05-30T13:30:00"),LocalDateTime.parse("2020-05-30T17:30:00"),3))
    val result = AvailabilityInterval.fromXml(validInterval)
    assert(expected === result)
  }

  /**
    * Negative preference validation
    */
  test("Negative AvailabilityInterval.fromXml") {
    //Failure(new IllegalArgumentException("Availability start cannot be after end"))
    assertThrows[IllegalArgumentException] { // Result type: Assertion
      AvailabilityInterval.fromXml(invalidInterval)
    }
  }
  /**
    * Time not correct, end before start for example
    */
  test("Negative 2 AvailabilityInterval.fromXml") {
    //Failure(new IllegalArgumentException("Availability start cannot be after end"))
    assertThrows[IllegalArgumentException] { // Result type: Assertion
      AvailabilityInterval.fromXml(invalidInterval2)
    }
  }
}