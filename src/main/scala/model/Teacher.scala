package model


case class Teacher(override val id: String,override val name: String,  override val availabilityIntervals : List[AvailabilityInterval]) extends Resource (id, name, availabilityIntervals){

}
object Teacher extends ResourceFactory {
  override def createResource(id: String, name: String, availabilities: List[AvailabilityInterval]): Option[Teacher] = Some(Teacher(id,name,availabilities))
}