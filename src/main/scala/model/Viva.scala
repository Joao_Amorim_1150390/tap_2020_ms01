package model


import scala.util.{Failure, Success, Try}
import scala.xml.{Elem, Node}

case class Viva(title: String, student: String, jury: List[VivaResource]) {
  val president = jury.find(_.role.equals(President)).get
  val adviser = jury.find(_.role.equals(Adviser)).get
  val coadvisers = jury.filter(_.role.equals(Coadviser))
  val supervisors = jury.filter(_.role.equals(Supervisor))
}

object Viva{

  private val teacherIsPresident: String => Boolean = _.matches("T[0-9]{3}")
  private val teacherIsAdviser: String => Boolean = _.matches("T[0-9]{3}")
  private val hasOnePresident: List[VivaResource] => Boolean = _.filter(j => j.role.equals(President)).size == 1
  private val hasOneAdviser: List[VivaResource] => Boolean = _.filter(j => j.role.equals(Adviser)).size == 1
  def nodeToViva(title: String, student: String, jury: List[VivaResource]): Try[Viva] = {
    if(jury.find(j=>j.role.equals(President)).isDefined && (!teacherIsPresident(jury.find(j => j.role.equals(President)).get.resource))) {
    Failure(new IllegalArgumentException("Node president has to be a teacher in viva"))
  } else if(jury.find(j=>j.role.equals(Adviser)).isDefined && (!teacherIsAdviser(jury.find(j => j.role.equals(Adviser)).get.resource))) {
     Failure(new IllegalArgumentException("Node adviser has to be a teacher in viva"))
   }else if(!hasOnePresident(jury)) {
      Failure(new IllegalArgumentException("Node president is empty/undefined in viva"))
   } else if(!hasOneAdviser(jury)) {
      Failure(new IllegalArgumentException("Node adviser is empty/undefined in viva"))
   } else {
      Success(Viva(title,student,jury))
   }
  }
  def fromXml(elem: Node): Try[Viva] = {
    val title = (elem \@ "title").toString()
    val student = (elem \@ "student").toString()
    val jury = elem.child.collect {case e: Elem => e}.map(VivaResource.fromXml(_))
    this.nodeToViva(title, student, jury.filter(_.isSuccess).map(_.get.get).toList)
  }
}