package model

import java.time.{Duration, LocalDateTime}
import scala.util.{Failure, Success, Try}
import scala.xml.Elem

case class AvailabilityInterval (start: LocalDateTime, end: LocalDateTime, preference: Int){

  /**
    * To check if availability overlaps current availability of comparison
    * @param availability
    * @return Boolean
    */
  def overlaps(availability: AvailabilityInterval): Boolean = {
    availability.start.isAfter(this.start) && availability.start.isBefore(this.end) && availability.end.isAfter(this.end)
  }

  /**
    * To create an availability interval composed by an availability that overlaps another availability taking into
    * consideration the duration of a Viva, overlaping without any problem when it comes to the duration of availabilities
    * @param availability
    * @param duration
    * @return AvailabilityInterval
    */
  def overlaps(availability: AvailabilityInterval, duration: Duration): Option[AvailabilityInterval] = {
    if(this.overlaps(availability) && Duration.between(availability.start,this.end).compareTo(duration) >= 0){
      AvailabilityInterval.createInterval(availability.start,this.end,this.preference+availability.preference).get
    }else{
      None
    }
  }

  /**
    * Check if availability is contained in another Availability Interval (finishing at same time, starting after)
    * @param availability
    * @return Boolean
    */
  def finishedBy(availability: AvailabilityInterval): Boolean = {
    availability.start.isAfter(this.start) && availability.end.isEqual(this.end)
  }

  /**
    * Composition of availabilities if they respect the duration of the Viva
    * @param availability
    * @param duration
    * @return AvailabilityInterval
    */
  def finishedBy(availability: AvailabilityInterval, duration: Duration): Option[AvailabilityInterval] = {
    if(this.finishedBy(availability) && Duration.between(availability.start,this.end).compareTo(duration) >= 0){
      AvailabilityInterval.createInterval(availability.start,this.end,this.preference+availability.preference).get
    }else{
      None
    }
  }
  /**
    * Verify if an availability it's contained inside another
    * @param availability
    * @return Boolean
    */
  def contains(availability: AvailabilityInterval): Boolean = {
    availability.start.isAfter(this.start) && availability.end.isBefore(this.end)
  }

  /**
    * Return availability if the new availability is contained inside the present one and
    * respects the Viva duration
    * @param availability
    * @param duration
    * @return AvailabilityInterval
    */
  def contains(availability: AvailabilityInterval, duration: Duration): Option[AvailabilityInterval] = {
    if(this.contains(availability) && Duration.between(availability.start,availability.end).compareTo(duration) >= 0){
      AvailabilityInterval.createInterval(availability.start,availability.end,this.preference+availability.preference).get
    }else{
      None
    }
  }

  /**
    * Check if the new availability starts at the same time and ends after, and the existing one respects the duration
    * @param availability
    * @return Boolean
    */
  def starts(availability: AvailabilityInterval): Boolean = {
    availability.start.isEqual(this.start) && availability.end.isAfter(this.end)
  }

  /**
    * Return availability if the new availability starts at same time as present and the existing
    * respects the Viva duration
    * @param availability
    * @param duration
    * @return AvailabilityInterval
    */
  def starts(availability: AvailabilityInterval, duration: Duration): Option[AvailabilityInterval] = {
    if(this.starts(availability) && Duration.between(availability.start,this.end).compareTo(duration) >= 0){
      AvailabilityInterval.createInterval(availability.start,this.end,this.preference+availability.preference).get
    }else{
      None
    }
  }

  /**
    *  Check if the availabilities represent the same time interval
    * @param availability
    * @return Boolean
    */
  def equals(availability: AvailabilityInterval): Boolean = {
    availability.start.isEqual(this.start) && availability.end.isEqual(this.end)
  }

  /**
    * Return availability if the new availability is equal to the present one respects the Viva duration
    * @param availability
    * @param duration
    * @return AvailabilityInterval
    */
  def equals(availability: AvailabilityInterval, duration: Duration): Option[AvailabilityInterval] = {
    if(this.equals(availability) && Duration.between(availability.start,this.end).compareTo(duration) >= 0){
      AvailabilityInterval.createInterval(availability.start,this.end,this.preference+availability.preference).get
    }else{
      None
    }
  }
  /**
    * Check and compare all validations above with present AvailabilityInterval and the one to be added in both ways,
    * as the new AvailabilityInterval can be sooner or later
    * @param availability
    * @param duration
    * @return AvailabilityInterval
    */
  def crossValidations(availability: AvailabilityInterval, duration: Duration): Option[AvailabilityInterval] = {
    List(this.overlaps(availability,duration), this.finishedBy(availability,duration), this.contains(availability, duration),
      this.starts(availability, duration), this.equals(availability,duration),availability.overlaps(this,duration),
      availability.finishedBy(this,duration),availability.contains(this, duration),
      availability.starts(this, duration)).find(_.isDefined).getOrElse(None)
  }
}
object AvailabilityInterval {
  private val isPreferenceValid: (Int) => Boolean = preference => preference >= 0
  private val areDatesValid: (LocalDateTime, LocalDateTime) => Boolean = (start, end) => start.isBefore(end)
  private val isValid: (LocalDateTime, LocalDateTime, Int) => Boolean = (start, end, preference) => areDatesValid(start, end) && isPreferenceValid(preference)

  def createInterval(start: LocalDateTime, end: LocalDateTime, preference: Int): Try[Option[AvailabilityInterval]] = {
    if (isValid(start, end, preference)){
      Success(Some((AvailabilityInterval(start, end, preference))))
    }else {
      Failure(new IllegalArgumentException("Availability start cannot be after end"))
    }
  }
  def fromXml(elem: Elem): Option[AvailabilityInterval] = {
    val start = LocalDateTime.parse((elem \@ "start").toString())
    val end = LocalDateTime.parse((elem \@ "end").toString())
    val preference = (elem \@ "preference").toString().toInt
    this.createInterval(start, end, preference).get
  }
}