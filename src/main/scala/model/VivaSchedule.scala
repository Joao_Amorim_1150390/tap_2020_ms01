package model

import java.time.format.DateTimeFormatter

import scala.util.{Failure, Success, Try}
import scala.xml.Elem

case class VivaSchedule(viva: Viva, availabilityInterval: AvailabilityInterval) {
  val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
  private def toXml(resources: List[Resource]): Elem = {
    <viva student={this.viva.student} title={this.viva.title}
          start={this.availabilityInterval.start.format(this.formatter)} end={this.availabilityInterval.end.format(this.formatter)} preference={this.availabilityInterval.preference.toString}>
      <president name={resources.find(_.id.equals(this.viva.president.resource)).get.name}/>
      <adviser name={resources.find(_.id.equals(this.viva.adviser.resource)).get.name}/>
      {this.viva.coadvisers.map(x => <coadviser name={resources.find(_.id.equals(x.resource)).get.name}/>)}
      {this.viva.supervisors.map(x => <supervisor name={resources.find(_.id.equals(x.resource)).get.name}/>)}
    </viva>
  }
}

object VivaSchedule{
  def create(viva: Viva, availabilityInterval: AvailabilityInterval): Try[VivaSchedule] = Success(VivaSchedule(viva,availabilityInterval))
  def toXml(scheduledVivas: List[Try[VivaSchedule]], resources: List[Resource]): Try[Elem] = {
    val failure = scheduledVivas.find(_.isFailure)
    if(failure.isDefined) {
      failure.get match {
        case Failure(exception) => Failure(exception)
      }
    } else {
      Success(
        <schedule xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../schedule.xsd" totalPreference={scheduledVivas.map(_.get.availabilityInterval.preference).sum.toString}>
          {scheduledVivas.map(_.get.toXml(resources))}
        </schedule>)
    }
  }
}

