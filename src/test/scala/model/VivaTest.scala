package model

import model._
import org.scalatest.funsuite.AnyFunSuite

import scala.util.Failure


class VivaTest extends AnyFunSuite {
  val listVivaResources= List(VivaResource("T001",President), VivaResource("T002",Adviser))
  val errorlistVivaResources= List(VivaResource("E001",President), VivaResource("T002",President))
  val node = <viva student="Student 001" title="Title 1">
    <president id="T001"/>
    <adviser id="T002"/>
    <supervisor id="E002"/>
  </viva>

  val errorNode = <viva student="Student 001" title="Title 1">
    <president id="E001"/>
    <adviser id="T002"/>
    <supervisor id="T003"/>
  </viva>
  val errorNode2 = <viva student="Student 001" title="Title 1">
    <president id="T001"/>
    <adviser id="T002"/>
    <adviser id="T003"/>
  </viva>

  test("Viva.nodeToViva") {
    val expected = new Viva("Title 1", "Student 001", List(VivaResource("T001",President), VivaResource("T002",Adviser)))
    val result = Viva.nodeToViva("Title 1","Student 001",listVivaResources).get
    assert(expected === result)
  }

  test("Negative Viva.nodeToViva") {
    //Failure(new IllegalArgumentException("Node president has to be a teacher in viva"))
    val expected =  false
    val result = Viva.nodeToViva("Test01","1150390",errorlistVivaResources).isSuccess
    assert(expected === result)
  }

  test("Positive Viva.fromXml") {
    val expected = new Viva("Title 1", "Student 001", List(VivaResource("T001",President), VivaResource("T002",Adviser),VivaResource("E002",Supervisor)))
    val result = Viva.fromXml(node).get
    assert( expected === result)
  }
  test("Negative 1 Viva.fromXml") {
    //Failure(new IllegalArgumentException("Node president has to be a teacher in viva"))
    val expected = false
    val result = Viva.fromXml(errorNode).isSuccess
    assert( expected === result)
  }

  test("Negative 2 Viva.fromXml") {
    //Failure(new IllegalArgumentException("Node adviser is empty/undefined in viva"))
    val expected = false
    val result = Viva.fromXml(errorNode2).isSuccess
    assert( expected === result)
  }


}
