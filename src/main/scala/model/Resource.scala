package model

import java.time.{Duration}
import scala.xml.{Elem, Node}

abstract class Resource (val id: String, val name: String, val availabilityIntervals: List[AvailabilityInterval]){

  /**
    * For all the unboooked availabilities of the resource, see if they can be added to list of viva availabilities being filled!
    * Duration parameter is being validated when calling crossValidations
    * @param bookedAvailabilities
    * @param availabilities
    * @param duration
    * @return List of Availability Intervals
    */
  def cross(bookedAvailabilities: List[AvailabilityInterval], availabilities: List[Option[AvailabilityInterval]],duration: Duration): List[Option[AvailabilityInterval]] = {
    val notBookedAvailabilities = this.getNotBookedAvailabilities(bookedAvailabilities)
    availabilities.filter(_.isDefined).map(_.get).foldLeft(List[Option[AvailabilityInterval]]()){
      case (listAvailabilities, availability) =>
        listAvailabilities ::: notBookedAvailabilities.map(availability.crossValidations(_,duration))
    }
  }
  /**
    * This method is divided into two processes:
    * First one to get clear intervals without any booking on top that can be picked at any moment
    * Second to check and create if possible sub intervals to have max efficiency and great use rate
    * @param bookedAvailabilities
    * @return list of Availability Intervals
    */
  def getNotBookedAvailabilities(bookedAvailabilities: List[AvailabilityInterval]): List[AvailabilityInterval] = {
    val notBookedAvailabilities = this.availabilityIntervals.foldLeft(List[AvailabilityInterval]()){
      case (listAvailabilities, availability) =>
        val bav = bookedAvailabilities.find(bav => availability.finishedBy(bav) || availability.contains(bav) || bav.starts(availability) || availability.equals(bav))
        if(bav.isDefined){
          listAvailabilities
        }else{
          listAvailabilities :+ availability
        }
    }
    val dividedAvailabilities = this.availabilityIntervals.foldLeft(List[AvailabilityInterval]()){
      case (listAvailabilities, availability) =>
        val bav = bookedAvailabilities.find(bav => availability.finishedBy(bav) || availability.contains(bav) || bav.starts(availability))
        if(bav.isDefined){
          listAvailabilities ::: List(AvailabilityInterval.createInterval(availability.start,bav.get.start,availability.preference),
            AvailabilityInterval.createInterval(bav.get.end,availability.end,availability.preference)).filter(_.isSuccess).map(_.get.get)
        }else{
          listAvailabilities
        }
    }
    notBookedAvailabilities ::: dividedAvailabilities
  }
}
/**
  * Trait to be used in subclasses Teacher and External
  */
trait ResourceFactory{
  def createResource(id: String, name: String, availabilities: List[AvailabilityInterval]): Option[Resource]
  def fromXml(elem: Node): Option[Resource] = {
    val id = (elem \@ "id").toString()
    val name = (elem \@ "name").toString()
    val availabilityIntervals = elem.child.collect {case e: Elem => e}.map(AvailabilityInterval.fromXml(_))
    this.createResource(id, name, availabilityIntervals.filter(_.isDefined).map(_.get).toList)
  }
}
