package model

import model.{President, VivaResource}
import org.scalatest.funsuite.AnyFunSuite

import scala.util.{Failure, Success}

class VivaResourceTest extends AnyFunSuite{

  val elem = <president id="T001"/>
  val errorElem1 = <president id="E001"/>
  val errorElem2 = <not_existing id="E001"/>

  test("VivaResource.createVivaResource") {
    val expected = Success(Some(VivaResource("T001", President)))
    val result = VivaResource.createVivaResource("T001",President)
    assert(expected === result)
  }

  test("Positive VivaResource.fromXml") {
    val expected = Success(Some(VivaResource("T001", President)))
    val result = VivaResource.fromXml(elem)
    assert( expected === result)
  }


  test("Negative 2 VivaResource.fromXml") {
    val expected = false
    val result = VivaResource.fromXml(errorElem2).isSuccess
    assert( expected === result)
  }
}
