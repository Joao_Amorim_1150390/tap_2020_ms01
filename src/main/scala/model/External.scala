package model


case class External(override val id: String,override val name: String,  override val availabilityIntervals : List[AvailabilityInterval]) extends Resource(id, name, availabilityIntervals){

}
object External extends ResourceFactory {
  override def createResource(id: String, name: String, availabilities: List[AvailabilityInterval]): Option[External] = Some(External(id,name,availabilities))
}
